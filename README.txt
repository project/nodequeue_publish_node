README
======

Nodequeue Publish Node is the submodule of Nodequeue.
It gives you the flexibility to show only Publish node in a nodequeue.

Installation:-
1. Install as general modules.
2. Now, while adding or editing nodequeue, you would see one checkbox says that
Check this if you need to show only published Node in the Nodequeue.
3. After that save the nodequeue, you would able to see only published nodes
in the Nodequeue.
